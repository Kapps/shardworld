﻿module ShardWorld.ChunkGenerator;
public import ShardMath.Vector;
public import ShardWorld.LevelData;


/// Provides the base for a transformer that can generate chunks instead of simply transform them.
/// A ChunkGenerator can have zero or more ChunkTransformers applied afterwards.
class ChunkGenerator  {

public:	

	this(Vector2i MinSize, Vector2i MaxSize) {
		this._MinSize = MinSize;
		this._MaxSize = MaxSize;
	}

	/// Initializes the data for this level at the given location.
	/// Params:
	///		Level = The level to operate on.
	///		Location = The location within the level to start operating on, in unscaled level coordinates. Can be altered, but must remain within the original bounds.	
	/// Returns:
	///		Whether the chunk was able to be created; if not, a different generator is used.
	abstract bool CreateChunk(LevelData Level, ref Rectanglei Location);

	/// Gets the minimum or maximum size of the chunks this generator can generate.
	@property Vector2i MinSize() const {
		return _MinSize;
	}

	/// Ditto
	@property Vector2i MaxSize() const {
		return _MaxSize;
	}
	
private:
	Vector2i _MinSize, _MaxSize;
}