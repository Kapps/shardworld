﻿module ShardWorld.ChunkGeneratorCollection;
public import ShardWorld.ChunkGenerator;
private import ShardTools.List;


/// Provides a collection of ChunkGenerators for a level or chunk.
class ChunkGeneratorCollection  {

public:
	/// Initializes a new instance of the ChunkGeneratorCollection object.
	this() {
		
	}

	/// 
	void Add(ChunkGenerator Generator, Vector2i MinSize, Vector2i MaxSize, float Frequency, size_t MinOccurrences, size_t MaxOccurrences) {

	}
	
private:
	ChunkGenerator[] _Chunks;
}