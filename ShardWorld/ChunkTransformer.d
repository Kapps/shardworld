﻿module ShardWorld.ChunkTransformer;
public import ShardWorld.LevelTransformer;


/// Provides a transformer that operates on a single chunk of a level.
class ChunkTransformer : LevelTransformer {

public:
	/// Initializes a new instance of the ChunkTransformer object.
	this(LevelData Level) {
		super(Level);
	}

	/// Applies this transform onto the given LevelChunk.
	/// Params:
	/// 	Chunk = The chunk to transform.
	abstract void TransformChunk(LevelChunk Chunk);
	
private:
}