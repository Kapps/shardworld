﻿module ShardWorld.HeightTerrain;
private import std.algorithm;
private import std.math;
private import std.exception;
private import std.random;
private import ShardWorld.SkyRenderer;
private import ShardMath.Matrix;
private import ShardGraphics.ShaderImporter;
private import ShardContent.ContentLoader;
private import ShardGraphics.Effect;
private import ShardGraphics.VertexBufferObject;
private import std.array;
private import ShardWorld.HeightTerrainChunk;
private import ShardGraphics.VertexDeclaration;
private import ShardMath.Vector;

import ShardFramework.World.StaticGeometry;

/// A height-map based terrain rendered as a static geometry.
class HeightTerrain : StaticGeometry {
	

public:
	/// Initializes a new instance of the HeightTerrain object.
	this(BoundingBox Location, Texture HeightMap, Texture BlendMap, Texture NormalMap, size_t VertexScale) {		
		super(Location);
		this._HeightMap = HeightMap;
		this._NormalMap = NormalMap;
		this._BlendMap = BlendMap;
		this._VertexScale = VertexScale;
		// 255f because that's the max value we can have in a one-byte-per-channel height map.
		this._Scale = Vector3f(Location.Difference.X / _HeightMap.Width, Location.Difference.Y / 255f, Location.Difference.Z / _HeightMap.Height);
		GenerateVertexBuffer();		
		ChannelMapTarget = new RenderTarget(256, 256);
		InitializeChannelMap();
	}

	/// Creates the chunk of geometry that would encompass the given area.
	/// Params:
	/// 	Location = The area to encompass.
	override StaticGeometryChunk CreateChunk(BoundingBox Location) {
		return new HeightTerrainChunk(this, Location);
	}		

	/// Gets a value indiating whether this StaticGeometry supports chunks that encompass the same area.
	/// This can be useful to prevent multiple chunks being rendered by rendering one larger one instead.
	@property override bool SupportsDuplicateChunks() {
		// TODO: The problem with this is we can't have a different level of detail per node if we're rendering a large node.
		// And, can't do the hackish fix of edges being rendered at full detail for stitching.
		return true;
	}

	/// Indicates the level of detail to use for vertices.
	/// For example, with a VertexScale of 1, a 512x512 heightmap results in 512x512 vertices.
	/// With 2 however, it would result in 256x256 vertices, and tesselation used when possible.
	@property size_t VertexScale() const {
		return _VertexScale;
	}

	/// Gets the multiplier to scale the terrain by.
	/// A scale of 1 indicates no scaling. Scaling is Width * Length.
	@property Vector3f Scale() const {
		return _Scale;
	}

	/// Gets the Effect used to render the terrain.
	@property Effect Program() {
		return _Program;
	}

	/// Gets the height map used for this terrain.
	@property Texture HeightMap() {
		return _HeightMap;
	}

	/// Gets the normal map used for this terrain.
	@property Texture NormalMap() {
		return _NormalMap;
	}

	/// Gets the blend map used for this terrain.
	@property Texture BlendMap() {
		return _BlendMap;
	}
	
	/// Prepares this StaticGeometry for rendering. Called once per frame immediately before chunks are rendered.
	/// Params:
	/// 	Time = A snapshot of the Game's timing values.
	override void PrepareForRendering(GameTime Time) {
		GraphicsDevice.Vertices = VB;
		//GraphicsDevice.Indices = null;
		GraphicsDevice.Program = Program;
		GraphicsDevice.VertexElements = VertDec;
		Texture ChannelMap = ChannelMapTarget.GetTexture();

		Matrix4f World = Matrix4f.Identity;
		World.M41 = Location.Min.X;
		World.M42 = Location.Min.Y;
		World.M43 = Location.Min.Z;
		World.M11 = Scale.X;
		World.M22 = Scale.Y;
		World.M33 = Scale.Z;				
		World.M44 = 1;

		GraphicsDevice.Samplers[0].Value = _HeightMap;
		GraphicsDevice.Samplers[1].Value = _NormalMap;
		GraphicsDevice.Samplers[2].Value = _BlendMap;		
		GraphicsDevice.Samplers[3].Value = Weather.Current.CloudMap;		
		GraphicsDevice.Samplers[4].Value = ChannelMap;
		GraphicsDevice.Samplers[5].Value = RedTexture;
		GraphicsDevice.Samplers[6].Value = GreenTexture;
		GraphicsDevice.Samplers[7].Value = BlueTexture;		
		GraphicsDevice.Samplers[8].Value = AlphaTexture;		

		Shader VertShader = Program.GetShader(ShaderType.VertexShader);	
		VertShader.Parameters["World"].Value = World; //Matrix4f.CreateTranslation(Location.Min) * Matrix4f.CreateScale(Scale);
		VertShader.Parameters["HeightMapSize"].Value = Vector2f(_HeightMap.Width, _HeightMap.Height);
		VertShader.Parameters["BlendScale"].Value = Vector2f(_HeightMap.Width / cast(float)_BlendMap.Width, _HeightMap.Height / cast(float)_BlendMap.Height);
		VertShader.Parameters["NormalScale"].Value = Vector2f(_HeightMap.Width / cast(float)_NormalMap.Width, _HeightMap.Height / cast(float)_NormalMap.Height);
		VertShader.Parameters["HeightSampler"].Value = 0;		
		VertShader.Parameters["NormalSampler"].Value = 1;		
		
		Shader FragShader = Program.GetShader(ShaderType.PixelShader);		
		assert(ChannelMap.Width == ChannelMap.Height);
		FragShader.Parameters["ChannelMapSize"].Value = cast(float)ChannelMap.Width;				
		FragShader.Parameters["BlendSampler"].Value = 2;
		FragShader.Parameters["CloudSampler"].Value = 3;
		FragShader.Parameters["ChannelSampler"].Value = 4;						
		FragShader.Parameters["RedSampler"].Value = 5;
		FragShader.Parameters["BlueSampler"].Value = 6;
		FragShader.Parameters["GreenSampler"].Value = 7;
		FragShader.Parameters["AlphaSampler"].Value = 8;

		GraphicsDevice.State.CullMode = CullFace.CounterClockwise;
		GraphicsDevice.State.SetAlpha(BlendStyle.SourceAlpha, BlendStyle.InvertSource);
		//GraphicsDevice.State.SetAlpha(BlendStyle.None, BlendStyle.None);
	}

	private void InitializeChannelMap() {
		int Width = ChannelMapTarget.Width;
		int Height = ChannelMapTarget.Height;
		enforce(Width == Height, "Channel map must be square.");
		Color[] ColorData = new Color[ChannelMapTarget.Width * ChannelMapTarget.Height];		
		for(int y = 0; y < Height; y++) {
			for(int x = 0; x < Width; x++) {
				//ubyte Val = cast(ubyte)(dice(80, 20) * 255);
				ubyte Val = cast(ubyte)(uniform(0, 256));
				ColorData[y * Width + x].R = Val;
			}
		}
		Texture T = ChannelMapTarget.GetTexture();
		T.SetData(ColorData, Width, Height, BufferUseHint.Stream, BufferAccessHint.WriteOnly);
		T.MinFilter = TextureFilter.Nearest;
		T.MagFilter = TextureFilter.Nearest;
	}

	private static string MakeTextureMixin(string Color) {
		string TexName = Color ~ "Texture";
		string Result = "private Texture _" ~ TexName ~ "; ";
		Result ~= "
		/// Gets or sets the Texture used for the " ~ Color ~ " portion of the color map.
		public Texture " ~ TexName ~ "() {
			return _" ~ TexName ~ ";
		} ";
		Result ~= "public void " ~ TexName ~ "(Texture Value) {
			enforce(Value !is null, \"Unable to assign a null value to a color texture.\");
			if(_" ~ TexName ~ " is Value)
				return;			
			_" ~ TexName ~ " = Value;
		} ";
		return Result;
	}

	mixin(MakeTextureMixin("Red"));
	mixin(MakeTextureMixin("Blue"));
	mixin(MakeTextureMixin("Green"));
	mixin(MakeTextureMixin("Alpha"));
	
private:
	VertexDeclaration VertDec;
	VertexBuffer VB;
	Effect _Program;	
	Vector3f _Scale;
	size_t _VertexScale;	
	Texture _HeightMap, _BlendMap, _NormalMap;
	
	RenderTarget ChannelMapTarget;		

	private void GenerateVertexBuffer() {		
		size_t StepX = _VertexScale;
		size_t StepY = _VertexScale;
		size_t StartX = cast(size_t)round(Location.Min.X / Scale.X);
		size_t EndX = cast(size_t)round(Location.Max.X / Scale.X);
		size_t StartY = cast(size_t)round(Location.Min.Z / Scale.Z);
		size_t EndY = cast(size_t)round(Location.Max.Z / Scale.Z);
		size_t Width = (EndX - StartX);
		size_t Length = (EndY - StartY);		
		HeightTerrainVertex[] Vertices = uninitializedArray!(HeightTerrainVertex[])(Width * Length / (StepX * StepY));
		for(size_t y = 0; y < Length; y += min(StepY, Length - y)) {
			for(size_t x = 0; x < Width; x += min(StepX, Length - x)) {
				HeightTerrainVertex Vertex = void;
				Vertex.Position = Vector2f(x, y);
				Vertices[(y / StepY) * (Width / StepY) + (x / StepX)] = Vertex;				
			}
		}
		VB = new VertexBuffer();
		VB.SetData(Vertices, cast(uint)HeightTerrainVertex.sizeof, BufferUseHint.Static, BufferAccessHint.WriteOnly);		
		Shader VertShader = ContentLoader.Default.Load!(ShaderImporter)("Content\\Shaders\\TerrainVert");
		Shader FragShader = ContentLoader.Default.Load!(ShaderImporter)("Content\\Shaders\\TerrainFrag");
		_Program = new Effect(VertShader, FragShader);						
		VertDec = VertexDeclaration.CreateForShader!HeightTerrainVertex(VertShader, ["Position":"InPosition"]);
	}

	/// A single vertex in a HeightTerrain.
	struct HeightTerrainVertex {
		public Vector2f Position;
	}
}