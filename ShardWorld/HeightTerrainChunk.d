﻿module ShardWorld.HeightTerrainChunk;
private import ShardGraphics.GraphicsDevice;
private import std.exception;
private import ShardMath.Rectangle;
private import std.array;
private import std.math;
private import ShardWorld.HeightTerrain;
private import ShardFramework.World.StaticGeometry;

import ShardFramework.World.StaticGeometryChunk;

/// A single chunk in a height-map based terrain.
class HeightTerrainChunk : StaticGeometryChunk {

public:
	/// Initializes a new instance of the HeightTerrainChunk object.
	this(StaticGeometry Parent, BoundingBox Location) {
		super(Parent, Location);
		this.Terrain = cast(HeightTerrain)Parent;		
		GenerateIndexBuffer();				
	}	

	private void GenerateIndexBuffer() {
		uint StartX = cast(uint)(Location.Min.X / Terrain.Scale.X / Terrain.VertexScale);
		uint EndX = cast(uint)(Location.Max.X / Terrain.Scale.X / Terrain.VertexScale);
		uint StartY = cast(uint)(Location.Min.Z / Terrain.Scale.Z / Terrain.VertexScale);
		uint EndY = cast(uint)(Location.Max.Z / Terrain.Scale.Z / Terrain.VertexScale);
		uint Width = (EndX - StartX);
		uint Length = (EndY - StartY);
		size_t StepX = Terrain.VertexScale;
		size_t StepY = Terrain.VertexScale;		
		size_t MaxX = Terrain.HeightMap.Width / Terrain.VertexScale;
		size_t MaxY = Terrain.HeightMap.Height / Terrain.VertexScale;
		Indices = uninitializedArray!(int[])((Width / StepX) * (Length / StepY) * 6);
		size_t Count = 0;
		for(uint y = StartY; y < EndY; y+= StepY) {
			for(uint x = StartX; x < EndX; x += StepX) {
				size_t AdderX = StepX, AdderY = StepY;
				while(y + AdderY >= MaxY)
					AdderY--;
				while(x + AdderX >= MaxX)
					AdderX--;
				int BotLeft = cast(int)(x + (y + AdderY) * MaxX);
				int BotRight = cast(int)((x + AdderX) + (y + AdderY) * MaxX);
				int TopLeft = cast(int)(x + y * MaxX);
				int TopRight = cast(int)((x + AdderX) + y * MaxX);
				Indices[Count++] = BotLeft;					
				Indices[Count++] = TopRight;
				Indices[Count++] = TopLeft;
				Indices[Count++] = BotLeft;
				Indices[Count++] = BotRight;
				Indices[Count++] = TopRight;
			}
		}
		assert(Count == Indices.length);		
		TopIndexBuffer = new IndexBuffer();
		TopIndexBuffer.SetData(Indices, 4, BufferUseHint.Static, BufferAccessHint.WriteOnly);
	}
	 
	/// Renders this chunk of geometry.
	/// Params:
	///		Time = A snapshot of the Game's current timing values.
	override void Render(GameTime Time) {						
		GraphicsDevice.Indices = TopIndexBuffer;		
		//GraphicsDevice.State.CullMode = CullFace.Clockwise;
		GraphicsDevice.State.PerformDepthTest = true;
		GraphicsDevice.DrawElements(RenderStyle.Triangles, Indices.length, ElementType.Int32);		
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);		
		//glDrawElements(GL_TRIANGLE_STRIP, Indices[LOD].length, GL_UNSIGNED_INT, cast(GLvoid*)Indices[LOD].ptr);
		//GraphicsDevice.DrawElements(RenderStyle.TriangleStrip, Indices[LOD].length, ElementType.Int32);
	}
	
private:
	IndexBuffer TopIndexBuffer;
	int[] Indices;
	HeightTerrain Terrain;
	Rectanglei Unscaled;

}