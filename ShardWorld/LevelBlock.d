﻿module ShardWorld.LevelBlock;
private import std.math;
public import ShardFramework.Components.Entity;
private import ShardTools.ExceptionTools;
private import ShardTools.ArrayOps;
public import ShardWorld.LevelData;

mixin(MakeException("AlreadySpecialException", "Attempted to mark a block as being special, but there was already a special object present on the block."));

/// Represents the lowest level block of data in a level.
/// This is the smallest scale that allows operations and adjustments inside a level.
class LevelBlock  {
	
	this(LevelData Level, size_t X, size_t Y) {
		this._Level = Level;
	}

	/// Gets the X or Y value of this block, in unscaled level coordinates.
	final @property uint X() const {
		return _X;
	}

	/// Ditto
	final @property uint Y() const {
		return _Y;
	}

	/// Gets or sets the height that this block is at.
	/// An exception is thrown if Height is changed after the block is made special.
	final @property uint Height() const {
		return _Height;
	}

	/// Ditto
	final @property void Height(uint Value) {
		if(_IsSpecial)
			throw new InvalidOperationException("Unable to adjust the height of a special block.");
		_Height = Value;
	}

	/// Gets the level that this block is a part of.
	final @property LevelData Level() {
		return _Level;
	}
	
	/// Each block may hold a special object (such as a building), but multiple of these are not allowed.
	/// This property indicates whether this block already holds a special object.
	/// A special block may not have it's height adjusted.
	@property bool IsSpecial() const {
		return _IsSpecial;
	}

	/// Indicates that this block now contains a special object.
	void MakeSpecial() {
		if(IsSpecial)
			throw new AlreadySpecialException();
		_IsSpecial = true;		
	}

	/// Adds the given entity to this block.
	void AddEntity(Entity e) {
		// TODO: Consider a way of specifying where in this block they are, or just do it automatically.
		// Also determine if need to rotate to terrain, but that's probably the physics engine's job.
		_Entities ~= e;
	}

	/// Adjusts the height of this block by the given amount.
	/// Underflow is checked, making the minimum height zero.
	void AdjustHeight(int Amount) {
		if(Amount < 0 && abs(Amount) > _Height)
			Height = 0;
		else
			Height = Height + Amount;		
	}

	/// Connects this block to the given block, if not already connected.
	/// Connected blocks are guaranteed to have a path to them, with at least one path between the two having no height difference of greater than MaxUnblockedSlope.	
	void ConnectTo(LevelBlock Block) {
		if(_ConnectedTo.Contains(Block))
			return;
		_ConnectedTo ~= Block;
		assert(!Block._ConnectedTo.Contains(this));
		Block._ConnectedTo ~= this;
	}	

private:
	uint _X, _Y;	
	LevelBlock[] _ConnectedTo;
	bool _IsSpecial;
	LevelData _Level;
	Entity[] _Entities;
	uint _Height;
}