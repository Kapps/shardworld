﻿module ShardWorld.LevelChunk;
private import std.exception;
private import std.algorithm;
public import ShardWorld.LevelData;
public import ShardWorld.LevelBlock;
public import ShardMath.Rectangle;


/// Represents a chunk of any number of blocks inside a level.
struct LevelChunk  {

public:

	this(LevelData Level, Rectanglei Location) {
		this._Level = Level;
		this._Location = Location;
		enforce(Level.Width >= Location.Right && Level.Height >= Location.Bottom && Location.X >= 0 && Location.Y >= 0);
	}

	/// Gets the level that this chunk is a part of.
	@property LevelData Level() {
		return _Level;
	}

	/// Gets the location of this chunk, in unscaled level coordinates.
	@property Rectanglei Location() const {
		return _Location;
	}

	/// Gets the average height of all of the blocks this chunk contains.
	@property float AverageHeight() {
		size_t Total;		
		foreach(LevelBlock Block; this)
			Total += Block.Height;
		return Total / cast(float)(Location.Width * Location.Height);
	}

	/// Flattens the blocks to the given height.
	/// This is more efficient than calling Squish(0).
	void Flatten(uint Height) {
		foreach(LevelBlock Block; this)
			Block.Height = Height;
	}	
	
	/// Squishes this chunk so there is at most HeightDifference between the lowest and highest chunks.
	void Squish(uint HeightDifference) {
		uint Average = cast(uint)AverageHeight;
		uint Min, Max;
		foreach(LevelBlock Block; this) {
			Min = min(Block.Height, Min);
			Max = max(Block.Height, Max);
		}	
		int MinOffset = 0, MaxOffset = 0;
		if(Max > Average + (HeightDifference / 2))
			Max = Average + (HeightDifference / 2);
		else
			MinOffset = (Average + (HeightDifference / 2)) - Max;
		if(Min < Average - (HeightDifference / 2))
			Min = Average - (HeightDifference / 2);
		else
			MaxOffset = Min - (Average - (HeightDifference / 2));
		Max = Average + (HeightDifference / 2) + MaxOffset;
		Min = Average - (HeightDifference / 2) - MinOffset;
		foreach(LevelBlock Block; this) {
			if(Block.Height > Max)
				Block.Height = Max;
			if(Block.Height < Min)
				Block.Height = Min;
		}
	}

	/// Adjusts the height of all blocks inside this chunk by the given amount.	
	void AdjustHeight(int Height) {
		foreach(LevelBlock Block; this)
			Block.AdjustHeight(Height);
	}

	LevelBlock opIndex(size_t X, size_t Y) {
		assert(X < _Location.Width);
		assert(Y < _Location.Height);
		return _Level[X + _Location.X, Y + _Location.Y];
	}

	int opApply(int delegate(LevelBlock) dg) {
		int Result = 0;
		for(size_t x = 0; x < _Location.Width; x++) {
			for(size_t y = 0; y < _Location.Height; y++) {
				if((Result = dg(this[x, y])) != 0)
					break;
			}
		}
		return Result;
	}
	
private:
	Rectanglei _Location;
	LevelData _Level;
}