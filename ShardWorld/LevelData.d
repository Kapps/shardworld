﻿module ShardWorld.LevelData;
private import std.exception;
public import ShardWorld.LevelChunk;
public import ShardMath.Rectangle;
public import ShardWorld.LevelBlock;


/// Provides the base data used to represent a single level in a terrain based game.
/// This class stores level data, such as the height map, foliage, and other user defined properties.
class LevelData {

public:
	/// Initializes a new instance of the LevelData object.
	this(size_t Width, size_t Height) {
		this._Width = Width;
		this._Height = Height;		
		this._Blocks = GenerateBlocks();
		enforce(_Blocks.length == _Width * _Height);
	}

	/// Gets the width or height of this level, prior to scaling.
	final @property size_t Width() const {
		return _Width;
	}

	/// Ditto
	final @property size_t Height() const {
		return _Height;
	}

	/// Gets a chunk encompassing the given location, in unscaled level coordinates.
	LevelChunk GetChunk(Rectanglei Location...) {
		return LevelChunk(this, Location);
	}

	LevelBlock opIndex(size_t X, size_t Y) {
		return _Blocks[Y * _Width + X];
	}

	LevelBlock opIndex(size_t i) {		
		return _Blocks[i];
	}

protected:

	/// Generates the blocks this level contains.
	LevelBlock[] GenerateBlocks() {
		LevelBlock[] Blocks = new LevelBlock[_Width * _Height];
		for(size_t x = 0; x < _Width; x++)
			for(size_t y = 0; y < _Height; y++)
				Blocks[y * _Width + x] = new LevelBlock(this, x, y);
		return Blocks;
	}
	
private:
	size_t _Width;
	size_t _Height;
	LevelBlock[] _Blocks;
}