﻿module ShardWorld.LevelGenerator;
public import ShardWorld.ChunkGenerator;
private import ShardTools.List;
public import ShardWorld.LevelData;
private import ShardMath.Vector;

/// A generator used to procedurally generate 3D heightmap based levels.
/// A level generator takes in one or more ChunkGenerators, each of which may have one or more ChunkGenerators.
/// The level is then split into chunks, and these chunks generated with the generators.
/// The generators then run zero or more detail passes to attempt to add detail to the level.
/// Finally, level passes are run which do things like ensure that connected blocks have a path between them.
class LevelGenerator  {

public:
	/// Initializes a new instance of the LevelGenerator object.
	this() {
		
	}

	/// Gets the top level chunk generators that this level generator can use.
	@property List!ChunkGenerator Generators() {
		return _Generators;
	}

	/// Indicates what the maximum size difference is between blocks before it's considered blocked.
	@property uint MaxUnblockedSlope() const {
		return _MaxUnblockedSlope;
	}

	/// Ditto
	@property void MaxUnblockedSlope(uint Value) {
		_MaxUnblockedSlope = Value;
	}

	/// Creates a new level of the given size.	
	/// Params:	
	/// 	LevelSize = The size of the level, prior to scaling.
	LevelData GenerateLevel(LevelType : LevelData)(size_t Width, size_t Height) {
		LevelType Level = new LevelType(Width, Height);		
		InitializeLevel(Level);
	}

	/// Initializes the level to a default randomly generated level, prior to applying any chunks.
	/// The default implementation simply generates a perlin noise heightmap.
	void InitializeLevel(LevelData Level) {
		
	}
	
private:
	List!ChunkGenerator _Generators;
	uint _MaxUnblockedSlope = 2;
}