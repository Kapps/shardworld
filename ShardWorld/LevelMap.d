﻿module ShardWorld.LevelMap;
private import ShardGraphics.Texture;
public import ShardWorld.LevelData;


/// Represents a single map inside a level, such as the height data or normal data.
/// Params:
///		T = The type of the elements within the map.
class LevelMap(T)  {

public:
	/// Initializes a new instance of the LevelMap object.
	this(LevelData Level) {
		this._Level = Level;
		this._Data = new T[Level.Width * Level.Height];
	}

	/// Gets the underlying data for this level map.
	/// This does not return const in order to allow altering of elements, but altering the array itself is not allowed.
	final @property T[] Data() {
		return _Data;
	}

	/// Gets the level owning this map.
	final @property LevelData Level() {
		return _Level;
	}

	/// Gets the width or height of this map.
	final @property size_t Width() const {
		return _Level.Width;
	}

	/// Ditto
	final @property size_t Height() const {
		return _Level.Height;
	}

	final T opIndex(size_t X, size_t Y) {
		return this[Y * Width + X];
	}

	T opIndex(size_t Index) {
		return _Data[Index];
	}

	final void opIndexAssign(size_t X, size_t Y, T Value) {
		this[Y * Width + X] = Value;
	}

	void opIndexAssign(size_t Index, T Value) {
		this._Data[Index] = Value;
	}
	
	/// Creates a texture with the data contained by this map, in the most common format for this map unless specified otherwise.
	abstract Texture CreateTexture();
	
private:
	T[] _Data;
	LevelData _Level;
}
