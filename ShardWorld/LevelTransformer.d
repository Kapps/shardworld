﻿module ShardWorld.LevelTransformer;
public import ShardWorld.LevelChunk;
public import ShardWorld.LevelData;


/// Provides the base class for a transformer used to transform a single part of the level.
class LevelTransformer  {

public:
	/// Initializes a new instance of the LevelTransformer object.
	this(LevelData Level) {
		
	}

	/// Gets the level being transformed.
	@property LevelData	Level() {
		return _Level;
	}
	
private:
	LevelData _Level;
}