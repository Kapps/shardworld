﻿module ShardWorld.SkyRenderer;
public import ShardFramework.World.Weather;
private import ShardGraphics.TextureImporter;
private import std.math;
private import std.array;
private import std.random;
private import ShardTools.ArrayOps;
private import std.algorithm;
private import ShardFramework.Camera;
private import ShardMath.Matrix;
private import ShardGraphics.ShaderImporter;
private import ShardGraphics.ModelBatch;
private import ShardFramework.GameComponent;
private import ShardGraphics.ModelImporter;
private import ShardContent.ContentLoader;
private import ShardGraphics.Model;
private import ShardFramework.GameTime;
private import ShardFramework.Components.System;


/// A GameComponent used to render the sky for the world.
class SkyRenderer : GameComponent {

public: 
	// Note: This is a GameComponent because otherwise we'd have one for each Scene; bad.

	// Implementation:
	//	No sky texture. Just a colour determined by time of day.
	//	Procedural cloud map. Not stored, just generated each time.
	//	Atmospheric scattering, again based off dynamic clouds and no sky texture.
	//	Because of the procedural nature, every level looks different each time you play depending on sky.
	//	Sky casts shadow on ground. Have a fixed sky height, and determine overlap, then determine where that is on ground, then shadow it.
	//	Perhaps allow clouds to have more or less depth, as part of the cloud map (more white for example), indicating how much a shadow to do, or how grey the cloud is.
	//	Take into consideration weather; stormy means greyer clouds.
	/// Initializes a new instance of the SkyRenderer object.
	this() {				
		SkyModel = ContentLoader.Default.Load!(ModelImporter)("Content\\Models\\Dome2");
		Shader SkyVert = ContentLoader.Default.Load!(ShaderImporter)("Content\\Shaders\\SkyVert");
		Shader SkyFrag = ContentLoader.Default.Load!(ShaderImporter)("Content\\Shaders\\SkyFrag");
		SkyEffect = new Effect(SkyVert, SkyFrag);								
		foreach(ModelMesh Mesh; SkyModel.Meshes) {
			foreach(ModelMeshPart Part; Mesh.Parts) {
				/+VertexElement[] Elements = Part.ActiveDeclaration.Elements.dup;
				int Pos = Part.ActiveEffect.GetShader(ShaderType.VertexShader).Parameters["InNormals"].Position;
				size_t Index = IndexOf!"a.Position == b"(Elements, Pos);				
				Elements = Elements.remove(Index);
				VertexDeclaration Dec = new VertexDeclaration(Elements);+/
				VertexDeclaration Dec = Part.ActiveDeclaration;
				Part.AssignEffect(SkyEffect, Dec);
			}
		}
		NoiseTarget = new RenderTarget(800, 600, true);
		NoiseVertex[] NoiseVertices = [
			NoiseVertex(Vector3f(-1, -1, -1), Vector2f(0, 0)),
			NoiseVertex(Vector3f(1, -1, -1), Vector2f(1, 0)),
			NoiseVertex(Vector3f(1, 1, -1), Vector2f(1, 1)),
			NoiseVertex(Vector3f(-1, 1, -1), Vector2f(0, 1))
		];
		ushort[] Indices = new ushort[24];
		foreach(i, ref index; Indices)
			index = cast(ushort)i;
		IndexBuffer NoiseIBO = new IndexBuffer();
		NoiseIBO.SetData(Indices, 2, BufferUseHint.Static, BufferAccessHint.WriteOnly);
		VertexBuffer NoiseVBO = new VertexBuffer();
		NoiseVBO.SetData(NoiseVertices, cast(uint)NoiseVertex.sizeof, BufferUseHint.Static, BufferAccessHint.WriteOnly);
		InitializeStaticMap(64);
		Shader NoiseVert = ContentLoader.Default.Load!(ShaderImporter)("Content\\Shaders\\CloudVert");
		Shader NoiseFrag = ContentLoader.Default.Load!(ShaderImporter)("Content\\Shaders\\CloudFrag");
		Effect NoiseEffect = new Effect(NoiseVert, NoiseFrag);
		VertexDeclaration NoiseDec = VertexDeclaration.CreateForShader!(NoiseVertex)(NoiseVert, ["Position":"InPosition", "TexCoords":"InTexCoords"]);
		NoiseModel = Model.CreateSinglePart(NoiseVBO, NoiseIBO, NoiseEffect, NoiseDec);
		Viewport.Changed.Add(&OnViewportChanged);		
		// TODO: Abstract this stuff into a noisemap. Maybe.
	}	

	void OnViewportChanged() {
		/+if(NoiseTarget) {
			destroy(NoiseTarget);
			NoiseTarget = null;
		}
		NoiseTarget = new RenderTarget();
		GraphicsErrorHandler.AssertErrors();+/
	}
	
	/// Draws this GameComponent.
	/// This is generally called by the Game, and should not be called manually.
	/// Params:
	///		Time = A snapshot of the Game's current timing values.
	override void Draw(GameTime Time) {
		GraphicsDevice.State.CullMode = CullFace.None;
		GraphicsDevice.State.PerformDepthTest = false;
		GeneratePerlinNoise(Time);
		Vector3f CameraPos = Camera.Default.Position;
		float Scale = Camera.Default.FarPlane - Camera.Default.NearPlane;
		//Scale = 2;
		Scale /= 2;		
		//Matrix4f Transform = Matrix4f.CreateTranslation(0, -0.4f, 0) * Matrix4f.CreateScale(Scale) * Matrix4f.CreateTranslation(CameraPos);		
		//Matrix4f Transform = Quaternion.FromYawPitchRoll(0, -1.57f, 0).ToMatrix();
		Matrix4f Transform = Matrix4f.CreateTranslation(CameraPos.X, CameraPos.Y - 4f, CameraPos.Z);
		Transform.M11 *= 10;
		Transform.M22 *= 10;
		Transform.M33 *= 10;
		/+Transform.M41 = CameraPos.X;
		Transform.M42 = CameraPos.Y;
		Transform.M43 = CameraPos.Z;+/
		//Transform = Transform * Matrix4f.CreateScale(0.5f, 0.5f, 0.5f);
		//Transform *= Matrix4f.CreateRotationX
		//Transform = Transform * Matrix4f.CreateRotationY(-1.57f);
		//Matrix4f Transform = Matrix4f.CreateScale(Scale);
		//Transform = Matrix4f.CreateTranslation(0, 0.3f, 0) * Matrix4f.CreateScale(Scale) * Matrix4f.CreateTranslation(CameraPos);				
		foreach(ModelMesh Mesh; SkyModel.Meshes) {
			Matrix4f MeshWorld = Mesh.ParentBone.AbsoluteTransform * Transform;
			foreach(ModelMeshPart Part; Mesh.Parts) {
				Shader VertShader = Part.ActiveEffect.GetShader(ShaderType.VertexShader);
				VertShader.Parameters["World"].Value = MeshWorld;	
			//	VertShader.Parameters["Projection"].Value = Camera.Default.ProjectionMatrix;			
				GraphicsDevice.Samplers[0].Value = NoiseTarget.GetTexture();				
				Shader FragShader = Part.ActiveEffect.GetShader(ShaderType.PixelShader);				
				FragShader.Parameters["CloudSampler"].Value = 0;				
				FragShader.Parameters["TopColor"].Value = Weather.Current.SkyColor;
				FragShader.Parameters["BottomColor"].Value = Weather.Current.HorizonColor;				
				//FragShader.Parameters["CloudColor"].Value = Weather.Current.CloudColor;				
				//FragShader.Parameters["CloudOpacity"].Value = Weather.Current.CloudOpacity;							
				ModelBatch.Default.RenderMeshPart(Part, ModelRenderMode.Immediate);
			} 
		}
		
		GraphicsDevice.State.PerformDepthTest = true;
		GraphicsDevice.State.CullMode = CullFace.CounterClockwise;
		//ModelBatch.Default.RenderModel(SkyModel, Matrix4f.Identity, ModelRenderMode.Immediate);
	}

private:	
	Model SkyModel;		
	Vector3f _SunDirection;
	Effect SkyEffect;

	// Clouds:
	RenderTarget NoiseTarget;		
	Texture NoiseMap;
	Model NoiseModel;	

	private void GeneratePerlinNoise(GameTime Time) {			
		auto OldTarget = GraphicsDevice.RenderTargets.Set(0, NoiseTarget);		
		GraphicsDevice.Clear(Color.Purple);		

		ModelMeshPart Part = NoiseModel.Meshes[0].Parts[0];
		assert(NoiseModel.Meshes.length == 1 && NoiseModel.Meshes[0].Parts.length == 1);
		Shader FragShader = Part.ActiveEffect.GetShader(ShaderType.PixelShader);
		FragShader.Parameters["NoiseMapSampler"].Value = 0;
		FragShader.Parameters["Time"].Value = cast(float)(Time.Total.Seconds / 333);
		FragShader.Parameters["Overcast"].Value = Weather.Current.Overcast;
		FragShader.Parameters["WindDirection"].Value = Weather.Current.WindDirection;	
		FragShader.Parameters["NoiseOffset"].Value = Weather.Current.CloudModifier;
		//FragShader.Parameters["CloudOpacity"].Value = Weather.Current.CloudOpacity;
		GraphicsDevice.Samplers[0].Value = NoiseMap;		
		//ModelBatch.Default.RenderMeshPart(Part, ModelRenderMode.Immediate);
		GraphicsDevice.Program = Part.ActiveEffect;
		GraphicsDevice.Vertices = Part.Vertices.VBO;
		GraphicsDevice.Indices = Part.Indices.VBO;
		GraphicsDevice.VertexElements = Part.ActiveDeclaration;
		GLsizei Size = Part.Vertices.Size, Offset = Part.Vertices.Offset;
		GLsizei ElementSize = 2;
		glDrawElements(cast(GLenum)RenderStyle.Quads, Size / ElementSize / 4, GL_UNSIGNED_SHORT, cast(void*)(Offset / ElementSize / Size / 4));
		GraphicsDevice.RenderTargets.Set(0, OldTarget);
		Weather.Current.CloudMap = NoiseTarget.GetTexture();
	}

	private void InitializeStaticMap(int Resolution) {
		uint rnd = unpredictableSeed;
		Color[] ColorData = uninitializedArray!(Color[])(Resolution * Resolution);
		for(int y = 0; y < Resolution; y++) {
			for(int x = 0; x < Resolution; x++) {
				float Value = (Noise(x, y, rnd) + 1) / 2f;
				Color c = Color(Vector3f(Value));
				ColorData[y * Resolution + x] = c;
			}
		}
		NoiseMap = new Texture();
		NoiseMap.SetData(ColorData, Resolution, Resolution, BufferUseHint.Stream, BufferAccessHint.WriteOnly);				
	}

	float Noise(int x, int y, int random) {
		int n = x + (y * 57) + (random * 131);
		n = (n << 13) ^ n;		
		return (1.0f - ((n * (n * n * 15731 + 789221) +
            1376312589) & 0x7fffffff) * 0.000000000931322574615478515625f);
	}

	private struct NoiseVertex {
		Vector3f Position;
		Vector2f TexCoords;
		this(Vector3f Position, Vector2f TexCoords) {
			this.Position = Position;
			this.TexCoords = TexCoords;
		}
	}
}